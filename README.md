### This is a test project for **MindGeek** by  Daniel Andrei

* Server runs on port *_3001_* by default
* Client runs on port *_3000_* by default

Requirements:

* node v8.9.4
* npm v5.6.0

Instructions for both **server** and **client**

  * npm i
  * npm start

## Running the app

After starting both server and client access:

http://localhost:3000

### Notes

#### Developing Notes

* Total time allocated was ~9 hours (server took around ~5, while client was done in ~3, documentation and cleanup for about 1 hour)
* No tests are implemented
* Client was built using `create-react-app` because it would have taken a long time to config and bundle the boilerplate for the app, I used that time to structure a basic view for the items
* Server fetches data from json on each `/api/v1/items` call
* There's an API for the images `/api/v1/image` which is called with a query param `url` to return the base64 data of the image served by that `url`
* The server fetches the image from cache (filesystem) by key (url). If not available, it will fetch from `url`, cache it and return the base64 for it to the API
* Images that are not available at the provided `url` will not be displayed at all, the client has a basic check for base64 image

#### Application Notes

* Items **DO NOT** list all the details, I took the liberty to list only the most significant ones
