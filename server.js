import express from 'express';
import fs from 'fs';
import path from 'path';
import request from 'request';
import cors from 'cors';

import { getImage } from './images';

const app = express();
app.use(cors());
const router = express.Router();

const APP_PORT = process.env.PORT || 3001;
const DATA_URL = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';

// Retrieve data from URL
const fetchData = async url => {
  return await new Promise((resolve, reject) => {
    request({ url, json: true }, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        resolve(body);
      } else {
        console.error('Data server is down ', error);
        reject(error);
      }
    });
  });
};

// Api for retrieving all items
router.get('/api/v1/items', async function(req, res) {
  const data = await fetchData(DATA_URL);

  res.json(data);
});

// Api for retrieving an image
// Try to read image from cache, if it fails : fetch image from url, cache it and send it to the client.
// If it succeeds, send it to the client directly
router.get('/api/v1/image', async function(req, res) {
  const image = await getImage(req.query.url);

  res.json(image);
});

app.use('/', router);
// Listen on app port
app.listen(APP_PORT);
