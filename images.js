import path from 'path';
import base64Img from 'base64-img';
import fs from 'fs';
import md5 from 'md5';

// Default options to be used by images module
// TODO: make it configurable
const options = {
  dir: path.join(__dirname, 'cache/'),
  ext: '.cache',
};

const getGoogleProxy = url => {
  return `https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&url=${url}`;
};

const getPath = (fileName, options) => {
  return `${options.dir}${md5(fileName)}${options.ext}`;
};

// Write file to buffer
const writeFile = async (fileName, data) => {
  return await new Promise(resolve => {
    fs.writeFile(getPath(fileName, options), data, error => {
      resolve(data);
    });
  });
};

// Check if directory exists
const directoryExists = dir => {
  return fs.existsSync(dir);
};

// Read file, first check for directory
const readFile = async image => {
  if (!directoryExists(options.dir)) {
    fs.mkdirSync(options.dir);
  }

  return await new Promise(resolve => {
    fs.readFile(getPath(image, options), async (error, results) => {
      if (error) {
        // File does not exists, fetch, cache and retrieve it
        resolve(await get(image));
      } else {
        // File exists, convert buffer back to utf8 string
        resolve(results.toString('utf8'));
      }
    });
  });
};

// Fetch image by url, resolve with image data or url if it fails
const fetchImage = async url =>
  await new Promise(resolve => {
    base64Img.requestBase64(getGoogleProxy(url), (error, res, body) => {
      if (res.statusCode.toString() == '200') {
        resolve(body);
      } else {
        resolve(url);
      }
    });
  });

async function get(image) {
  return await new Promise(async resolve => {
    const data = await fetchImage(image);
    writeFile(image, data);

    resolve(data);
  });
}

// Retrieves a single image if in cache, if not it will fetch, store and return from cache
export async function getImage(image) {
  return await readFile(image);
}
