const SERVER = 'http://localhost:3001';
const API_BASE = '/api/v1';

export const FETCH_ITEMS_URL = `${SERVER}${API_BASE}/items`;
export const FETCH_IMAGE_URL = `${SERVER}${API_BASE}/image`;
