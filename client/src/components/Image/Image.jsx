import React from 'react';
import axios from 'axios';

import './Image.css';

import { FETCH_IMAGE_URL } from '../API';

const IMAGE_TYPE_HEIGHT_MAP = {
  card: 260,
  keyArt: 200,
};

export default class Image extends React.Component {
  state = {};

  componentDidMount() {
    axios.get(`${FETCH_IMAGE_URL}?url=${this.props.url}`).then(({ status, data }) => {
      if (status.toString() === '200' && data && typeof data === 'string' && data.startsWith('data:image/jpeg;')) {
        this.setState({
          dataSource: data,
        });
      }
    });
  }

  scaleImage = () => {
    const { width, height, type } = this.props;
    const ratio = Math.fround(width / height);

    return {
      height: IMAGE_TYPE_HEIGHT_MAP[type],
      width: Math.round(IMAGE_TYPE_HEIGHT_MAP[type] * ratio),
    };
  };

  render() {
    const { type } = this.props;
    const { dataSource } = this.state;
    const { width, height } = this.scaleImage();

    if (!dataSource) {
      return null;
    }

    return <img className="image" src={dataSource} width={width} height={height} />;
  }
}
