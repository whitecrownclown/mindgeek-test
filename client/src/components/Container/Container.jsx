import React from 'react';
import axios from 'axios';

import { FETCH_ITEMS_URL } from '../API';
import Item from '../Item/Item';

export default class Container extends React.Component {
  state = {};

  componentDidMount() {
    axios.get(FETCH_ITEMS_URL).then(response => {
      this.setState({ data: response.data });
    });
  }

  render() {
    const { data = [] } = this.state;

    return (
      <div>
        {data.map(item => {
          return <Item key={item.id} item={item} />;
        })}
      </div>
    );
  }
}
