import React from 'react';
import Image from '../Image/Image';

import './Item.css';

const CARD_IMAGE_INDEX = 5;

export default class Item extends React.Component {
  goToUrl = url => {
    window.location = url;
  };

  listActors = actors => {
    return actors.map(actor => {
      return <span>{actor.name}</span>;
    });
  };

  render() {
    const {
      body,
      cardImages = [],
      keyArtImages = [],
      headline,
      synopsis,
      cast,
      url,
      directors,
      quote,
    } = this.props.item;

    return (
      <div className="item">
        <a className="item_title" href={url}>
          {headline}
          <br />
          <div className="headline_image">
            {[cardImages[CARD_IMAGE_INDEX]].map(image => {
              return <Image type="card" width={image.w} height={image.h} url={image.url} />;
            })}
          </div>
          <div className="quote">{quote}</div>
        </a>
        <div className="row">
          <div className="keyArtImages">
            {keyArtImages.map(image => {
              return <Image type="keyArt" width={image.w} height={image.h} url={image.url} />;
            })}
          </div>
          <div className="synopsis">
            <p className="synopsis_title">Synopsis</p>
            <p className="synopsis_body">{synopsis}</p>
          </div>
          <div className="directors">
            <p className="cast_title">Directors</p>
            {this.listActors(directors)}
          </div>
          <div className="cast">
            <p className="cast_title">Cast</p>
            {this.listActors(cast)}
          </div>
        </div>
      </div>
    );
  }
}
